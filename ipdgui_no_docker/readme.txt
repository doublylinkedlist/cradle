Author: Xiaoli Yan
Email: xyan11@uic.edu

1. Install MongoDB Community Edition: https://docs.mongodb.com/manual/administration/install-community/

2. Start an instance of MongoDB, then restore the database content from db_seed/db_dump, check MongoDB website for commands and tutorials

3. Install Python environment according to your system/platform. We recommend installing Anaconda with Python 3 on Windows: https://www.anaconda.com/download/

For MacOs and Linux users, you can choose to install Anaconda with Python 3 or other distributions of Python 3. 

4. Create a Python 3 virtual environment and open a command prompt with the environment activated. 

5. Install all required packages by enter command: pip install -r requirements.txt

6. Start a Jupyter notebook server by enter command: jupyter notebook

7. Total number of entries: 61328

Materials Project: 12769
unary: 465
binary: 10725
ternary: 1579
entries with surface data: 17

OQMD: 48559
unary: 734
binary: 36489
ternary: 11336
entries with surface data: 0