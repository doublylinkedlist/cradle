for /f "usebackq delims=" %%x in (`docker ps -a -f "ancestor=ipdgui_web" -q`) do (docker kill %%x && docker stop %%x)
for /f "usebackq delims=" %%x in (`docker ps -a -f "ancestor=mongo:3.4.5" -q`) do (docker kill %%x && docker stop %%x)
for /f "usebackq delims=" %%x in (`docker ps -a -f "ancestor=ipdgui_db" -q`) do (docker kill %%x && docker stop %%x)

for /f "usebackq delims=" %%x in (`docker container ls -a -f "ancestor=ipdgui_web" -q`) do (docker container rm %%x)
for /f "usebackq delims=" %%x in (`docker container ls -a -f "ancestor=mongo:3.4.5" -q`) do (docker container rm %%x)
for /f "usebackq delims=" %%x in (`docker container ls -a -f "ancestor=ipdgui_db" -q`) do (docker container rm %%x)

docker-compose rm -f
for /f "usebackq delims=" %%x in (`docker volume ls -qf "dangling=true"`) do (@echo off && docker volume rm %%x>NUL)
docker image prune -f
docker-compose build
timeout/t 1 /nobreak>nul
start cmd /k "docker-compose up"
@echo off
echo db_seed is not launched
:label0
docker ps -f "NAME=ipdgui_db_seed_1"|find /i "ipdgui_db_seed_1" >NUL
if errorlevel 1 (timeout/t 1 /nobreak>nul && goto :label0)
echo.
echo ##################################################
echo ##################################################
echo db_seed is launched, database is being initialized
echo ##################################################
echo ##################################################
echo.
echo.
echo db is not ready
:label
docker ps -f "NAME=ipdgui_db_seed_1"|find /i "ipdgui_db_seed_1" >NUL
if errorlevel 1 (goto :label1)
timeout/t 1 /nobreak>nul
goto :label
echo.
:label1
docker ps -a -f "exited=0"|find /i "ipdgui_db_seed_1" >NUL
if errorlevel 1 (timeout/t 1 /nobreak>nul && goto :label2)
echo.
echo ##################################################
echo ##################################################
ECHO db is initialized and ready for connection
echo ##################################################
echo ##################################################
echo.
docker exec -it ipdgui_web_1 bash ./static/jupyter_list.sh
for /f "delims=" %%x in (./static/jupyter_token.txt) do set jupyter_token=%%x
set jupyterurl=http://localhost:8890%jupyter_token:~19,-8%
start "" %jupyterurl%
goto :label3
:label2
echo ##################################################
echo ##################################################
echo db is not functioning, please restart the application
echo ##################################################
echo ################################################## 
PAUSE
:label3

