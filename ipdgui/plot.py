import math
import plotly.graph_objs as go

import numpy as np
from db_connect import *
import plotly


from IPython.display import IFrame
from pyqh3d import *

def visualize_chemdoodle(_cif, w=800, h=800, xm=1, ym=1, zm=1):
    
    cif = "\"" + _cif.replace("\n", "\\n").replace('"', "'") + "\""
        
    read_template = ""
    with open("./static/chemdoodle_template.html", "r") as read_file:
        read_template = read_file.read()
        
    txt_list = read_template.split("/* $$CIF_HANDLE$$ */\n")
    
    txt_list[1] = "var cif = " + cif + ";\n"
    
    write_txt = "/* $$CIF_HANDLE$$ */\n".join(txt_list)
    
    write_txt = write_txt.replace("$$WIDTH$$", str(w)).replace("$$HEIGHT$$", str(h))
    
    write_txt = write_txt.replace("$$XM$$", str(xm)).replace("$$YM$$", str(ym)).replace("$$ZM$$", str(zm))
    
    with open("./static/chemdoodle_visualize.html", "w") as write_file:
        write_file.write(write_txt)
    
    return IFrame('./static/chemdoodle_visualize.html', width=w+30, height=h+250)

def binary_plot(unitary, binary, ternary, dataSource, elements):
    elementsList = alphabetical(elements)
    allEntries = unitary + binary + ternary

    pointSet = []
    for i in range(0, len(allEntries)):
        pointSet.append([getCoord2d(getComposition(allEntries[i]['unit_cell_formula'], elementsList)), allEntries[i]['formation_energy_per_atom'], i, allEntries[i]['_id']])
    if pointSet == []:
        return "", "", "", []
    hull_w_id = qh2d(pointSet)
    hull = []
    stableList = []
    for stablePhase in hull_w_id:
        hull.append(stablePhase[0:3])
        stableList.append(stablePhase[3])
        
    _x = []
    _y = []
    _text = []
    for i in range(0, len(hull)):
        _x.append(hull[i][0])
        _y.append(hull[i][1])
        _text.append(allEntries[hull[i][2]]['full_formula'] + "_" + dataSource)

    _xx = []
    _yy = []
    _tt = []
    for j in range(0, len(pointSet)):
        if pointSet[j] not in hull_w_id:
            _xx.append(pointSet[j][0])
            _yy.append(pointSet[j][1])
            _tt.append(allEntries[pointSet[j][2]]['full_formula'] + "_" + dataSource)
    

    if dataSource == "OQMD":
        trace0 = go.Scatter(
            x = _x,
            y = _y,
            text = _text,
            mode = 'lines+markers',
            marker= dict(size= 10,
                            line= dict(width=1),
                            color= 'rgba(255, 0, 0, 1)',
                            opacity= 1
                           ),
            name = 'OQMD: Stable'
        )
        trace1 = go.Scatter(
            x = _xx,
            y = _yy,
            text = _tt,
            mode = 'markers',
            marker= dict(size= 4,
                            line= dict(width=1),
                            color= 'rgba(255, 0, 0, 1)',
                            opacity= 1
                           ),
                           
            name = 'OQMD: Metastable'
            #secondary_y=True,
        )
    else:
        trace0 = go.Scatter(
            x = _x,
            y = _y,
            text = _text,
            mode = 'lines+markers',
            marker= dict(size= 10,
                            line= dict(width=1),
                            color= 'rgba(0, 0, 255, 1)',
                            opacity= 1
                           ),
            name = 'MP: Stable'
        )
        trace1 = go.Scatter(
            x = _xx,
            y = _yy,
            text = _tt,
            mode = 'markers',
            marker= dict(size= 4,
                            line= dict(width=1),
                            color= 'rgba(0, 0, 255, 1)',
                            opacity= 1
                           ),
                           
            name = 'MP: Unstable'
        )

    ghost_trace0 = go.Scatter(trace0)
    ghost_trace0["yaxis"]="y2"
    ghost_trace0["xaxis"]="x2"
    ghost_trace0['name']= ""
    ghost_trace0['showlegend']=False
    ghost_trace1 = go.Scatter(trace1)
    ghost_trace1["yaxis"]="y2"
    ghost_trace1["xaxis"]="x2"
    ghost_trace1['name']= ""
    ghost_trace1['showlegend']=False
    pd_data = [trace0, trace1, ghost_trace0, ghost_trace1]
    
    pd_layout = {
        "title": {
            "text": dataSource + ' Phase Diagram',
            "font": {
                "family": 'Arial',
                "size": 30,
                "color":"rgb(0, 0, 0)",
            },
            "x": 0.4,
        },
        "xaxis1": {
            "title": elementsList[1] + "/(" + elementsList[0] + "+" + elementsList[1] + ") Composition",
            "titlefont": {
                "family": 'Arial',
                "size": 20,
                "color":"rgb(0, 0, 0)",
                
            },
            "tickfont": {
                "size": 20.0, 
                "color": "rgb(0, 0, 0)",
            }, 
            "showgrid": False, 
            "side": "bottom", 
            "type": "linear",
            "ticks": "inside", 
            "showline": True, 
            "mirror": "ticks", 
            "linecolor":"rgb(0, 0, 0)",
            "tickcolor": "rgb(0, 0, 0)",
            #"zeroline": False,
        },
        "xaxis2": {
            
            "tickfont": {
                "size": 20.0, 
                "color": "rgb(0, 0, 0)",
            }, 
            "showgrid": False, 
            "side": "top", 
            "type": "linear",
            "ticks": "inside", 
            "showline": True, 
            "mirror": "ticks", 
            "linecolor":"rgb(0, 0, 0)",
            "tickcolor": "rgb(0, 0, 0)",
            #"zeroline": False,
        },
        
        "yaxis1": {
            "title": 'Formation Energy(eV/atom)',
            "titlefont": {
                "family": 'Arial',
                "size": 20,
                "color": "rgb(0, 0, 0)",
                
            },
            "tickfont": {
                "size": 20.0, 
                "color": "rgb(0, 0, 0)",
            }, 
            "showgrid": False, 
            "side": "left", 
            "type": "linear",
            "ticks": "inside", 
            "showline": True, 
            "mirror": "ticks", 
            "linecolor":"rgb(0, 0, 0)",
            "tickcolor": "rgb(0, 0, 0)",
            "domain": [0.0, 1.0],
            #"zeroline": True,
            #"zerolinecolor": "rgb(0, 0, 0)",
        },
        
        "yaxis2": {
            #"visible": True,
            #"title": "Y-axis",
            "tickfont": {
                "size": 20.0, 
                "color": "rgb(0, 0, 0)",
            }, 
            "showgrid": False, 
            "side": "right", 
            "type": "linear",
            "ticks": "inside", 
            "showline": True, 
            "mirror": "ticks", 
            "linecolor":"rgb(0, 0, 0)",
            "tickcolor": "rgb(0, 0, 0)",
            #"anchor": "free",
            #"tickmode": "auto",
            "domain": [0.0, 1.0],
            #"zeroline": True,
            #"zerolinecolor": "rgb(0, 0, 0)",
        },
        "legend":{
            "font": {
                "family": 'Arial',
                "size": 20,
                "color": "rgb(0, 0, 0)",
                
            },
            "x": 1.1,
            
        },
    
        "plot_bgcolor": "rgb(255, 255, 255)",
    }
    pd_fig = go.Figure(data=pd_data, layout=pd_layout)
    
    #url_1 = py.iplot(pd_fig, filename= elements + '_Phase_Diagram_' + dataSource, auto_open=False)
  
    if dataSource == "OQMD":
        labels=['Stable Entries','Metastable Entries']
    else: 
        labels=['Stable Entries','Unstable Entries']   
    values=[len(_x), len(_xx)]
    colors = ['rgba(0, 0, 255, 0.3)', 'rgba(255, 0, 0, 0.3)']

    trace=go.Pie(labels=labels,values=values,marker={'colors': colors})
    pie_chart_fig = go.Figure()
    pie_chart_fig.add_trace(trace)
    
    #url_2 = py.iplot([trace], filename= elements + '_Pie_Chart_' + dataSource, auto_open=False)

    xx = []
    yy = []
    for entry in allEntries:
        yy.append(entry['formation_energy_per_atom'])
        xx.append(entry['full_formula'])
    bar_chart_fig = go.Figure()
    bar_chart_fig.add_trace(go.Bar(
                x=xx,
                y=yy
        ))
    #data = [go.Bar(x=xx, y=yy)]
    #url_3 = py.iplot(data, filename= elements + '_Formation_Energy_Per_Atom_' + dataSource, auto_open=False)
    return pd_fig, pie_chart_fig, bar_chart_fig, stableList, pd_data

def binary_plot_combined(mpd, oqmdd, elements):
    elementsList=alphabetical(elements)
    pd_layout = {
        "title": {
            "text": 'Combined Phase Diagram',
            "font": {
                "family": 'Arial',
                "size": 30,
                "color":"rgb(0, 0, 0)",
            },
            "x": 0.4,
        },
        
        "xaxis1": {
            "title": elementsList[1] + "/(" + elementsList[0] + "+" + elementsList[1] + ") Composition",
            "titlefont": {
                "family": 'Arial',
                "size": 20,
                "color":"rgb(0, 0, 0)",
                
            },
            "tickfont": {
                "size": 20.0, 
                "color": "rgb(0, 0, 0)",
            },  
            "showgrid": False, 
            "side": "bottom", 
            "type": "linear",
            "ticks": "inside", 
            "showline": True, 
            "mirror": "ticks", 
            "linecolor":"rgb(0, 0, 0)",
            "tickcolor": "rgb(0, 0, 0)",
            #"zeroline": False,
        },
        "xaxis2": {
            
            "tickfont": {
                "size": 20.0, 
                "color": "rgb(0, 0, 0)",
            }, 
            "showgrid": False, 
            "side": "top", 
            "type": "linear",
            "ticks": "inside", 
            "showline": True, 
            "mirror": "ticks", 
            "linecolor":"rgb(0, 0, 0)",
            "tickcolor": "rgb(0, 0, 0)",
            #"zeroline": False,
        },
        
        "yaxis1": {
            "title": 'Formation Energy(eV/atom)',
            "titlefont": {
                "family": 'Arial',
                "size": 20,
                "color": "rgb(0, 0, 0)",
                
            },
            "tickfont": {
                "size": 20.0, 
                "color": "rgb(0, 0, 0)",
            }, 
            "showgrid": False, 
            "side": "left", 
            "type": "linear",
            "ticks": "inside", 
            "showline": True, 
            "mirror": "ticks", 
            "linecolor":"rgb(0, 0, 0)",
            "tickcolor": "rgb(0, 0, 0)",
            "domain": [0.0, 1.0],
            #"zeroline": True,
            #"zerolinecolor": "rgb(0, 0, 0)",
        },
        
        "yaxis2": {
            #"visible": True,
            #"title": "Y-axis",
            "tickfont": {
                "size": 20.0, 
                "color": "rgb(0, 0, 0)",
            }, 
            "showgrid": False, 
            "side": "right", 
            "type": "linear",
            "ticks": "inside", 
            "showline": True, 
            "mirror": "ticks", 
            "linecolor":"rgb(0, 0, 0)",
            "tickcolor": "rgb(0, 0, 0)",
            #"anchor": "free",
            #"tickmode": "auto",
            "domain": [0.0, 1.0],
            #"zeroline": True,
            #"zerolinecolor": "rgb(0, 0, 0)",
        },
        


        "plot_bgcolor": "rgb(255, 255, 255)",
        "legend":{
            "font": {
                "family": 'Arial',
                "size": 20,
                "color": "rgb(0, 0, 0)",
                
            },
            "x": 1.1,
            
        }
    }
    
    ghost_trace0 = go.Scatter(mpd[0])
    ghost_trace0["yaxis"]="y2"
    ghost_trace0["xaxis"]="x2"
    ghost_trace0['name']= ""
    ghost_trace0['showlegend']=False
    ghost_trace1 = go.Scatter(mpd[1])
    ghost_trace1["yaxis"]="y2"
    ghost_trace1["xaxis"]="x2"
    ghost_trace1['name']= ""
    ghost_trace1['showlegend']=False
    ghost_trace2 = go.Scatter(oqmdd[0])
    ghost_trace2["yaxis"]="y2"
    ghost_trace2["xaxis"]="x2"
    ghost_trace2['name']= ""
    ghost_trace2['showlegend']=False
    ghost_trace3 = go.Scatter(oqmdd[1])
    ghost_trace3["yaxis"]="y2"
    ghost_trace3["xaxis"]="x2"
    ghost_trace3['name']= ""
    ghost_trace3['showlegend']=False
        
    pd_data = [ghost_trace0, ghost_trace1, ghost_trace2, ghost_trace3]+mpd+oqmdd
    fig = go.Figure(data=pd_data, layout=pd_layout)
    return fig
    #return py.iplot(fig, filename= elementsList[1] + "-" + elementsList[0] + '_Phase_Diagram_Combined', auto_open=False)

        

def ternary_plot(unitary, binary, ternary, dataSource, elements):
    elementsList = alphabetical(elements)
    if len(elementsList) < 3:
        return "", "", ""
    A = dict()
    for i in range(0, len(unitary)):
        currElement = unitary[i]['elements'][0]
        if currElement not in A:
            A[currElement] = [dict(unitary[i])]
        else:
            A[currElement].append(dict(unitary[i]))

    UU = []
    for key, value in A.items():
        minFE = {'formation_energy_per_atom': math.inf}
        for i in range(0, len(value)):
            if value[i]['formation_energy_per_atom'] < minFE['formation_energy_per_atom']:
                minFE = dict(value[i])
        UU.append(dict(minFE))

    B = dict()
    for i in range(0, len(binary)):
        currElementList = '-'.join(binary[i]['elements'])
        if currElementList not in B:
            B[currElementList] = [dict(binary[i])]
        else:
            B[currElementList].append(dict(binary[i]))

    BB = []
    for key, value in B.items():
        curr2DPointSet = []
        system = value[:]
        currEList = key.strip().split("-")
        for u in UU:
            if u['elements'][0] in currEList:
                system.append(u)
        
        for i in range(0, len(system)):
            coord = getCoord2d(getComposition(system[i]['unit_cell_formula'],currEList))
            curr2DPointSet.append([coord, system[i]['formation_energy_per_atom'], i, system[i]['material_id'], key])
        
        curr2DHull = qh2d(curr2DPointSet)
        
        for j in range(0, len(curr2DHull)):
            currList = B[curr2DHull[j][4]]
            for k in range(0, len(currList)):
                if currList[k]['material_id'] == curr2DHull[j][3]:
                    if currList[k]['nelements'] == 2:
                        BB.append(dict(currList[k]))
                        break

    allEntries = unitary + binary + ternary
    pointSet = []
    for i in range(0, len(allEntries)):
        coords = getCoordinate(getComposition(allEntries[i]['unit_cell_formula'], elementsList))
        pointSet.append([coords[0], coords[1], allEntries[i]['formation_energy_per_atom'], allEntries[i]['full_formula'], allEntries[i]['nelements']])    
        
    hullInput = UU + BB + ternary
    hullInputSet = []
    
    
    ceiling = dict()
    ceiling['formation_energy_per_atom'] = math.inf
    for u in UU:
        if u['formation_energy_per_atom'] < ceiling['formation_energy_per_atom']:
            ceiling = dict(u)
            
    for i in range(0, len(hullInput)):
        if hullInput[i]['nelements'] == 3:
            if hullInput[i]['formation_energy_per_atom'] > ceiling['formation_energy_per_atom']:
                continue
        coords = getCoordinate(getComposition(hullInput[i]['unit_cell_formula'], elementsList))
        hullInputSet.append([coords[0], coords[1], hullInput[i]['formation_energy_per_atom'], hullInput[i]['full_formula'], hullInput[i]['nelements'], hullInput[i]['_id']])    

    hull_w_id = qh3d(hullInputSet)

    hull = []
    stableList = []
    for stablePhase in hull_w_id:
        hull.append(stablePhase[0:5])
        stableList.append(stablePhase[5])
        
    minE = np.inf
    minIndex = -1
    for i in range(0, len(hull)):
        if hull[i][2] < minE:
            minE = hull[i][2]
            minIndex = i

    _x = []
    _y = []
    _z = []
    _text = []
    for i in range(0, len(hull)):
        _x.append(hull[i][0])
        _y.append(hull[i][1])
        _z.append(hull[i][2])
        _text.append(hull[i][3])

    _xx = []
    _yy = []
    _zz = []
    _tt = []
    for i in range(0, len(pointSet)):
        if pointSet[i] not in hull:
            _xx.append(pointSet[i][0])
            _yy.append(pointSet[i][1])
            _zz.append(pointSet[i][2])
            _tt.append(pointSet[i][3])    
        
    sin15 = (sqrt(6) - sqrt(2)) / 4
    cos15 = (sqrt(6) + sqrt(2)) / 4
    A = [0, 0, 0]
    B = [cos15, sin15, 0]
    C = [sin15, cos15, 0]
    _A = [0, 0, minE]
    _B = [cos15, sin15, minE]
    _C = [sin15, cos15, minE]

    if dataSource == "OQMD":
        trace3 = go.Scatter3d(
            x=_xx,
            y=_yy,
            z=_zz,
            text = _tt,
            mode='markers',
            marker=dict(
                color='rgb(255, 0, 0)',
                size=4,
                symbol='circle',
                line=dict(
                    color='rgb(204, 204, 204)',
                    width=1
                ),
                opacity=0.9
            ),
            name='Metastable Phases', 
            showlegend=True
        )
    else:
        trace3 = go.Scatter3d(
            x=_xx,
            y=_yy,
            z=_zz,
            text = _tt,
            mode='markers',
            marker=dict(
                color='rgb(255, 0, 0)',
                size=4,
                symbol='circle',
                line=dict(
                    color='rgb(204, 204, 204)',
                    width=1
                ),
                opacity=0.9
            ),
            name='Unstable Phases', 
            showlegend=True
        )

    trace0 = go.Scatter3d(
        x=_x,
        y=_y,
        z=_z,
        text = _text,
        mode='markers',
        marker=dict(
            color='rgb(0, 0, 255)',
            size=6,
            symbol='circle',
            line=dict(
                color='rgb(204, 204, 204)',
                width=1
            ),
            opacity=0.9
        ), 
        name='Stable Phases', 
        showlegend=True
    )

    #trace1 = go.Mesh3d(x=_x,y=_y,z=_z,color='rgba(0,205,0,0.6)',opacity=0.50, alphahull=0, name='Convex Hull', showlegend=True)
    trace1 = go.Mesh3d(x=_x,y=_y,z=_z,color='rgba(0,205,0,0.6)',opacity=0.50, alphahull=0, name='Convex Hull')
    trace2 = go.Scatter3d(
        x=[A[0], B[0], C[0], A[0], _A[0], _B[0], _C[0], _A[0], _B[0], B[0], C[0], _C[0]],
        y=[A[1], B[1], C[1], A[1], _A[1], _B[1], _C[1], _A[1], _B[1], B[1], C[1], _C[1]],
        z=[A[2], B[2], C[2], A[2], _A[2], _B[2], _C[2], _A[2], _B[2], B[2], C[2], _C[2]],
        showlegend=False,
        text=['', '', '', '', elementsList[0], elementsList[1], elementsList[2], '', '', '', '', ''],
        textfont=dict(
            family='Arial',
            size=25,
            color='#000000'
        ),
        mode='lines+text',
        line=dict(
            color='#000000',
            width=1
        )
    )   
    data = [trace0, trace1, trace2, trace3]
    layout = go.Layout(
        title = dict(
            text=dataSource + ": " + elements + " Phase Diagram",
            font= dict(
                family= 'Arial',
                size= 30,
                color="rgb(0, 0, 0)",
            ),
            x= 0.4,
        ),
        legend = dict(
            font= dict(
                family= 'Arial',
                size= 20,
                color="rgb(0, 0, 0)",
            ),
            x= 1.1,
        ),
        margin=dict(
            l=0,
            r=0,
            b=0,
            t=80
        ),
        scene = dict(
            xaxis=dict(
                autorange=True,
                showgrid=False,
                zeroline=False,
                showline=False,
                title='',
                ticks='',
                showticklabels=False,
                showbackground=False,
            ),
            yaxis=dict(
                autorange=True,
                showgrid=False,
                zeroline=False,
                showline=False,
                title='',
                ticks='',
                showticklabels=False,
                showbackground=False,
            ),
            zaxis = dict(
                title='Formation Energy (eV/atom)',
                    titlefont=dict(
                    family='Arial',
                    size=20,
                    color='#000000'
                ),
                nticks=8,
                tickfont=dict(
                    size=15, 
                    color= "rgb(0, 0, 0)",
                ), 
                range = [minE, 0],
                showgrid=False,
                zeroline=False,
                showline=False,
                showbackground=True,
                backgroundcolor="white",
                gridcolor="white",
            ),
        ),

    )
    fig1 = go.Figure(data=data, layout=layout)    

    #url_1 = py.iplot(fig, filename= elements + '_Phase_Diagram_' + dataSource, auto_open=False)
    
    labels=['Stable Entries','Unstable Entries']
    values=[len(_x), len(_xx)]
    colors = ['rgba(0, 0, 255, 0.6)', 'rgba(255, 0, 0, 0.6)']

    trace=go.Pie(labels=labels,values=values,marker={'colors': colors})
    fig2 = go.Figure()
    fig2.add_trace(trace)
    #url_2 = py.iplot([trace], filename= elements + 'mp-pie-chart' + dataSource, auto_open=False)

    xx = []
    yy = []
    for entry in allEntries:
        deltaE = entry['formation_energy_per_atom']
        yy.append(deltaE)
        xx.append(entry['full_formula'])

    #data = [go.Bar(x=xx, y=yy)]
    fig3 = go.Figure()
    fig3.add_trace(go.Bar(x=xx, y=yy))
    #url_3 = py.iplot(data, filename= elements + 'formation_energy_per_atom' + dataSource, auto_open=False)
    return fig1, fig2, fig3, stableList

# write the python dict data into readable javascript obj
def surfaces_parse(entry_dict):
    if 'surface_data' not in entry_dict: 
        print ("No surface information available for this structure!")
        return None
    surf_enrgy_data = ""
    for key, value in entry_dict['surface_data']['surfaces'].items():

        miller_index = key

        for layer_num, layer_info in value['slab']['layers'].items():
            if layer_info['surface_energy'] > 0:
                if value['slab']['number_of_layers'] > 1:
                    surf_enrgy_data = surf_enrgy_data + """{\n"""
                    surf_enrgy_data = surf_enrgy_data + """  'index_number': '""" + key + "(" + str(int(layer_num) + 1) + ")" + """',\n"""
                    surf_enrgy_data = surf_enrgy_data + """  'surface_energy': """ + str(layer_info['surface_energy']) + """,\n},\n"""

                elif value['slab']['number_of_layers'] == 1:
                    surf_enrgy_data = surf_enrgy_data + """{\n"""
                    surf_enrgy_data = surf_enrgy_data + """  'index_number': '""" + key + """',\n"""
                    surf_enrgy_data = surf_enrgy_data + """  'surface_energy': """ + str(layer_info['surface_energy']) + """,\n},\n"""
                    
                else:
                    break
            else:
                break
                
    return surf_enrgy_data

def visualize_surfaces(surf_enrgy_data):
    with open("./static/surfaces_template.html", "r") as read_file:
        read_template = read_file.read()
    
    txt_list = read_template.split("/* $$SURFACE_HANDLE$$ */\n")
    
    txt_list[1] = surf_enrgy_data
    
    write_txt = "/* $$SURFACE_HANDLE$$ */\n".join(txt_list)
    with open("./static/surfaces_visualize.html", "w") as write_file:
        write_file.write(write_txt)
    return

def single_surface_layer(entry_dict, miller_index, layer_number):
    if 'surface_data' not in entry_dict: 
        print ("No surface information available for this structure!")
        return None
    return entry_dict['surface_data']['surfaces'][miller_index]['slab']['layers'][str(int(layer_number) - 1)]