Author: Xiaoli Yan
Email: xyan11@uic.edu

1. Please install Docker Community Edition: https://www.docker.com/community-edition/, you will be required to register and then download the docker CE version. 

2. If you are using Windows, please install Docker Compose after Docker Community Edition is installed. Linux and Mac users can skip this step: https://docs.docker.com/compose/install/

3. After Docker & Docker Compose installation:
For Windows users:
Double click "cmd_docker_up.bat"

For Linux users:
Run script "linux_docker_up.sh"

For Mac users:
Run script "mac_docker_up.sh"

4. If the setup script runs well, you should see a Jupyter notebook opened in your default browser. 

5. Total number of entries: 61328

Materials Project: 12769
unary: 465
binary: 10725
ternary: 1579
entries with surface data: 17

OQMD: 48559
unary: 734
binary: 36489
ternary: 11336
entries with surface data: 0