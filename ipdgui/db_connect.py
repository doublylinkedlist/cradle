from pymongo import MongoClient
from math import sqrt
import numpy as np
import os
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
def alphabetical(s):
    
    e = s.split('-')
    e.sort()
    _e = []
    if len(e) > 1:
        i = 0
        _e.append(e[0])
        while i < len(e)-1:
            if e[i]==e[i+1]:
                i=i+1
                continue
            i=i+1
            _e.append(e[i])
    else:
        _e = e
    return _e
    
def getEntries(s):

    db = client.Magnesium
    mp = db.MaterialsProject
    
    c = mp.find({"elements":s})
    rt = []
    for i in range(0, c.count()):
        rt.append(c[i])
    return rt

def getEntries_oqmd(s):

    db = client.MgOQMD
    mp = db.oqmd
    
    c = mp.find({"elements":s})
    rt = []
    for i in range(0, c.count()):
        rt.append(c[i])
    return rt

def getEntrybyID(entry_id):

    db = client.Magnesium
    mp = db.MaterialsProject
    
    c = list(mp.find({"material_id":entry_id}))
    if len(c) == 0:
        print("No entry found by id: " + str(entry_id) + "\n")
        return []
    else:
        return c

def getEntrybyID_oqmd(entry_id):

    db = client.MgOQMD
    mp = db.oqmd
    
    c = list(mp.find({"material_id":entry_id}))
    
    if len(c) == 0:
        print("No entry found by id: " + str(entry_id) + "\n")
        return []
    else:
        return c

def getSpecies(s):
    _s = alphabetical(s)
    un = []
    bi = []
    te = []
    
    for i in range(0, len(_s)):
        un = un + getEntries([_s[i]])
    for i in range(0, len(_s)):
        for j in range(i+1, len(_s)):
            bi = bi + getEntries([_s[i],_s[j]])
    for i in range(0, len(_s)):
        for j in range(i+1, len(_s)):
            for k in range(j+1, len(_s)):
                te = te + getEntries([_s[i],_s[j],_s[k]])
    return un, bi, te

def getSpecies_oqmd(s):
    _s = alphabetical(s)
    un = []
    bi = []
    te = []
    
    for i in range(0, len(_s)):
        un = un + getEntries_oqmd([_s[i]])
    for i in range(0, len(_s)):
        for j in range(i+1, len(_s)):
            bi = bi + getEntries_oqmd([_s[i],_s[j]])
    for i in range(0, len(_s)):
        for j in range(i+1, len(_s)):
            for k in range(j+1, len(_s)):
                te = te + getEntries_oqmd([_s[i],_s[j],_s[k]])
    return un, bi, te

def getComposition(ucf, s):
    r = []
    for i in range(0, len(s)):
        if s[i] in ucf:
            r.append(ucf[s[i]])
        else:
            r.append(0)
    return r

def getCoordinate(r):
    sin15 = (sqrt(6) - sqrt(2)) / 4
    cos15 = (sqrt(6) + sqrt(2)) / 4
    tan15 = 2 - sqrt(3)
    cot15 = 2 + sqrt(3)

    rr = r[0] + r[1] + r[2]
    
    a = r[0] / rr
    b = r[1] / rr
    c = r[2] / rr

    a1 = [(1 - a) * sin15, (1 - a) * cos15]
    b1 = [b * cos15, b * sin15]
    c1 = [c * sin15, c * cos15]
    
    X = (cot15 * b1[0] + a1[0] + a1[1] - b1[1]) / (1 + cot15)
    Y = a1[1] + a1[0] - X

    return [X, Y]
    
def plane3Points(A, B, C):
    a = (B[1] - A[1]) * (C[2] - A[2]) - (C[1] - A[1]) * (B[2] - A[2])
    b = (B[2] - A[2]) * (C[0] - A[0]) - (C[2] - A[2]) * (B[0] - A[0])
    c = (B[0] - A[0]) * (C[1] - A[1]) - (C[0] - A[0]) * (B[1] - A[1]) 
    d = 0 - (a * A[0] + b * A[1] + c * A[2])
    if c > 0: 
        return -a, -b, -c, -d
    else:
        return a, b, c, d
    
def line2Points(x1, x2):
    a = x2[1] - x1[1]
    b = x1[0] - x2[0]
    c = x2[0] * x1[1] - x1[0] * x2[1]
    if b <= 0 :
        return a, b, c
    else:
        return -a, -b, -c

def qh2d(pointSet):
    # initialization
    P = pointSet[:]
    # sort by x coordinate
    P.sort(key=lambda point: point[0])
    # only take the lowest point for each x coordinate
    currMin = P[0]
    _P = []
    for i in range(1, len(P), 1):
        if P[i][0] == P[i - 1][0]:
            if P[i][1] < currMin[1]:
                currMin = P[i]
            if i == len(P) - 1:
                _P.append(currMin)
        else:
            _P.append(currMin)
            currMin = P[i]
            if i == len(P) - 1:
                _P.append(P[i])
    
    # find hull
    L = _P[0][:]
    R = _P[len(_P) - 1][:]
    currHull = [L, R]
    _P.remove(L)
    _P.remove(R)
    
    while len(_P) > 0:
        inside = [True] * len(_P)
        _PP = _P[:]
        _currHull = currHull[:]
        #print("currHull:", currHull)
        for j in range(1, len(currHull)):
            # choose edge
            a, b, c = line2Points(currHull[j], currHull[j - 1])
            maxDist = -np.inf
            maxDistP = [np.inf, np.inf, np.inf]
            
            # find max dist point to this edge
            for i in range(0, len(_P)):
                dist = (a * _P[i][0] + b * _P[i][1] + c) / sqrt(a * a + b * b)

                if dist >= 0:
                    if dist > maxDist:
                        maxDist = dist
                        maxDistP = _P[i][:]
            if maxDist >= 0 and maxDistP not in _currHull and maxDistP in _PP:
                _currHull.append(maxDistP)
                _currHull.sort(key=lambda point: point[0])
                _PP.remove(maxDistP)
                break

        for j in range(1, len(currHull)):
            a, b, c = line2Points(currHull[j], currHull[j - 1])    
            for i in range(0, len(_P)):
                dist = (a * _P[i][0] + b * _P[i][1] + c)
                if dist >= 0:
                    inside[i] = False
                
        for i in range(0, len(_P)):
            if inside[i] == True:
                if _P[i] in _PP:
                    _PP.remove(_P[i])
        _P = _PP[:]
        currHull = _currHull[:]
        
    currHull.sort(key=lambda point: point[0])
    return currHull
    
def getCoord2d(r):
    return r[0] / (r[0] + r[1])


def energyAboveHull2d(entry, hull):
    x = getCoord2d(getComposition(entry['unit_cell_formula'], elementsList))
    if x == hull[0][0]:
        delta_e = entry['formation_energy_per_atom'] - hull[0][1]
        return delta_e
    for i in range(1, len(hull)):
        if hull[i][0] == x:
            delta_e = entry['formation_energy_per_atom'] - hull[i][1]
            return delta_e
        elif hull[i - 1][0] < x and x < hull[i][0]:
            x1 = hull[i - 1][0]
            x2 = hull[i][0]
            y1 = hull[i - 1][1]
            y2 = hull[i][1]
            y = ((x - x2) * y1 + (x1 - x) * y2) / (x1 - x2)
            delta_e = entry['formation_energy_per_atom'] - y
            return delta_e
        else:
            continue

            
