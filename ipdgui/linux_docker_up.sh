docker kill $(docker ps -a -f "ancestor=ipdgui_web" -q);
docker kill $(docker ps -a -f "ancestor=ipdgui_db_seed" -q);
docker kill $(docker ps -a -f "ancestor=ipdgui_db" -q);
docker stop $(docker ps -a -f "ancestor=ipdgui_web" -q);
docker stop $(docker ps -a -f "ancestor=ipdgui_db_seed" -q);
docker stop $(docker ps -a -f "ancestor=ipdgui_db" -q);
docker-compose rm -f;
docker volume rm $(docker volume ls -qf "dangling=true");
docker image prune --force;
docker-compose build;

xterm -e "docker-compose up" &
echo "db_seed is not running. ";
while [ -z $(docker ps -f "NAME=ipdgui_db_seed_1" -q) ]; do
  sleep 1;
done;

echo "db_seed is running. ";
while ! [ -z $(docker ps -f "NAME=ipdgui_db_seed_1" -q) ]; do sleep 1; done;

if [ -z $(docker ps -a -f "exited=0" -f "NAME=ipdgui_db_seed_1" -q) ]; then echo "db not functioning, please restart the application"; else echo "db initialized, launching interface..."; docker exec -it ipdgui_web_1 bash ./static/jupyter_list.sh; jupyter_url=$(awk 'NR==2 {print substr($0,20,56)}' ./static/jupyter_token.txt); jupyter_url="http://localhost:8890$jupyter_url"; python -m webbrowser "$jupyter_url";
fi
